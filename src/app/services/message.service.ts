import { Injectable } from "@angular/core";
import swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    push(title?: string, message?: string, type?: any) {
        swal.fire(title, message, type);
    }
}
