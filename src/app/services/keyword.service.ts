import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class KeywordService {
    constructor(
        private httpClient: HttpClient,
    ) {}

    insertUser(sendData) {
        
        const url =  'http://localhost:8080/api/v1/user';
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url,  JSON.stringify(sendData), httpOptions).pipe(
            map(res => res)
        );
    }

    getUser(sendData) {
      
         const url =  'http://localhost:8080/api/v1/user/login';
         const httpOptions = {
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             })
         };
         return this.httpClient.post(url,  JSON.stringify(sendData), httpOptions).pipe(
             map(res => res)
         );
     }

     getOrderbyUserID(sendData) {
      
        const url =  'http://localhost:8080/api/v1/order/'+sendData;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url, httpOptions).pipe(
            map(res => res)
        );
    }

    getProductbyUserID(sendData) {
      
        const url =  'http://localhost:8080/api/v1/product/getall/byuser/'+sendData;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url, httpOptions).pipe(
            map(res => res)
        );
    }

    getProductbyID(sendData) {
      
        const url =  ' http://localhost:8080/api/v1/product/get/'+sendData;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url, httpOptions).pipe(
            map(res => res)
        );
    }


   
    createProduct(sendData) {

        const url =  'http://localhost:8080/api/v1/product';
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url,  JSON.stringify(sendData), httpOptions).pipe(
            map(res => res)
        );
    }

    getAllProduct() {
      
        const url =  'http://localhost:8080/api/v1/product/getall';
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url, httpOptions).pipe(
            map(res => res)
        );
    }

    generateOrder(sendData) {
      
        const url =  'http://localhost:8080/api/v1/order';
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.httpClient.post(url,  JSON.stringify(sendData),httpOptions).pipe(
            map(res => res)
        );
    }




    
}
