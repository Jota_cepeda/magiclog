import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { KeywordService } from '../services/keyword.service';
import { MessageService } from '../services/message.service';
 

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  table: boolean;
  product: boolean;
  main: boolean;
  disableGlobal: boolean;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  loginbool:boolean;
  userid : string
  

  constructor( 
    private formBuilder: FormBuilder,
    private httpClient: HttpClient, 
    private keywordService: KeywordService,
    private messageService: MessageService,
    ) { }

  ngOnInit() {
      this.loginbool = true;
      this.main= true;
      this.product= false;
      this.table= false;

      this.firstFormGroup = this.formBuilder.group({
        email: [ { value: '', disabled: this.disableGlobal }, [Validators.required, Validators.maxLength(50), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')] ],
        password: [{value: '', disabled: this.disableGlobal}, [Validators.required, Validators.maxLength(50)]],
        password_confirm: [{value: '', disabled: this.disableGlobal}, [ Validators.maxLength(50)]],
      } );


      this.secondFormGroup = this.formBuilder.group({
        email: [ { value: '', disabled: this.disableGlobal }, [Validators.required, Validators.maxLength(50), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')] ],
        password: [{value: '', disabled: this.disableGlobal}, [Validators.required, Validators.maxLength(50)]],
      } );

      this.getParams();



    }

  async showproduct() {
    this.main= false;
    this.product= true;
    this.table= false;
  }

  async showtable() {
    this.main= false;
    this.product= false;
    this.table= true;
  }
  
  async mainmenu() {
    this.clearFieldUser();
    this.main= true;
    this.product= false;
    this.table= false;
  }

  async createuser(){


    if (this.firstFormGroup.get('password').value  === this.firstFormGroup.get('password_confirm').value ){

      this.insertUser(this.firstFormGroup.get('email').value ,this.firstFormGroup.get('password').value ).then(() => {

      });

    }else{
      this.messageService.push('', 'The code of the passwords do not match' , 'warning');
      this.firstFormGroup.get('password').setValue('');
      this.firstFormGroup.get('password_confirm').setValue('');
    }
  }

  async clearFieldUser(){
    this.firstFormGroup.get('email').setValue('');
    this.firstFormGroup.get('password').setValue('');
    this.firstFormGroup.get('password_confirm').setValue('');

    this.secondFormGroup.get('email').setValue('');
    this.secondFormGroup.get('password').setValue('');

    this.firstFormGroup.reset();
    this.secondFormGroup.reset();
  
  }


  async logout(){
    sessionStorage.removeItem('userlogin');
    this.loginbool = false;
    this.messageService.push('', 'User log out' , 'success');
  }

  async login(){

    this.getUser(this.secondFormGroup.get('email').value ,this.secondFormGroup.get('password').value ).then(() => {
      this.loginbool = true;
    });
  }

  private insertUser( Email , Password) {
    return new Promise((resolve, reject) => {

      const sendData = {  email: Email, password: Password  };

      this.keywordService.insertUser(sendData).subscribe((res: any) => {
        this.clearFieldUser();
        this.messageService.push('', 'The user did create success.' , 'success');
        this.mainmenu();
        resolve(res);
      });

    });
  }

  private getUser( Email , Password) {
    return new Promise((resolve, reject) => {

      const sendData = {  email: Email, password: Password  };

      this.keywordService.getUser(sendData).subscribe((res: any) => {

      if (res === null || res === "null" ) {
        this.messageService.push('', 'errorrrrss' , 'warning');
      }else{
        this.loginbool = true;
        res.id
        sessionStorage.setItem('userlogin', res.id);
        this.clearFieldUser();
        this.mainmenu();
        this.messageService.push('', 'the combination email/password did success.' , 'success');
      }
        resolve(res);
      });
      debugger
    });
  }

  getParams() {
 
    const login = sessionStorage.getItem('userlogin');
    if (login != "" && login != null ){
        this.loginbool = true;
        this.userid = login;
     }else{
      this.loginbool = false;
     }
 
   }

}
