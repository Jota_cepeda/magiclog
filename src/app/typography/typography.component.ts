import { Component, OnInit } from '@angular/core';
import { _order } from '../models/model';
import { KeywordService } from '../services/keyword.service';
import { MessageService } from '../services/message.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {

  table: boolean;
  product: boolean;
  main: boolean;
  dataSource: _order[]=[];
  login : boolean
  userid : string
  firstFormGroup: FormGroup;
  disableGlobal: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private keywordService: KeywordService,
    private messageService: MessageService,
    
  ) { }

  ngOnInit() {
      this.main= true;
      this.product= false;
      this.table= false;

      this.firstFormGroup = this.formBuilder.group({
        Name: [ { value: '', disabled: this.disableGlobal }, [Validators.required, Validators.maxLength(50)]],
        Sku: [{value: '', disabled: this.disableGlobal}, [Validators.required, Validators.maxLength(50)]],
        Cantidad: [{value: '', disabled: this.disableGlobal}, [ Validators.maxLength(50)]],
      } );
 

       this.getParams();

  }
  async showproduct() {
    this.main= false;
    this.product= true;
    this.table= false;
  }

  async showtable() {
    this.main= false;
    this.product= false;
    this.table= true;
  }

  async mainmenu() {
    this.main= true;
    this.product= false;
    this.table= false;
  }

  getParams() {

    const login = sessionStorage.getItem('userlogin');
   
    if (login != "" && login != null ){
      this.userid = login;
      this.login=true;
      this.getproductbyuserid();
    }
 
   }
 
   async getproductbyuserid(){

    return new Promise(async (resolve, reject) => {

     await this.keywordService.getProductbyUserID(this.userid).subscribe((res: any) => {

      if (res === null || res === "null" ) {
        this.messageService.push('', 'errorrfdfdsfdfdfr' , 'warning');
      }else{
        this.dataSource=[];
        for (let i = 0; i < res.length; i++) {
          this.dataSource.push({ id: res[i].id ,name: res[i].name, sku: res[i].sku, cantidad : res[i].amount});
        }
      }
        resolve(res);
      });
      debugger
    });

  }

  createProduct(){

    return new Promise((resolve, reject) => {

      const sendData = {  userID : this.userid , Name: this.firstFormGroup.get('Name').value, Sku: this.firstFormGroup.get('Sku').value , amount: this.firstFormGroup.get('Cantidad').value.toString() ,  };
      this.keywordService.createProduct(sendData).subscribe((res: any) => {
        this.clearFieldUser();
        this.messageService.push('', 'The product did create success.' , 'success');
        this.mainmenu();
        this.getproductbyuserid();
        resolve(res);
      });

    });
  }

  async clearFieldUser(){
    this.firstFormGroup.get('Name').setValue('');
    this.firstFormGroup.get('Sku').setValue('');
    this.firstFormGroup.get('Cantidad').setValue('');
    this.firstFormGroup.reset();
  }


  deleteRow(e) {
    console.log("e");
    console.log(e);

    const kwtgToDelete = this.dataSource.find(data => data.id === e.id);
    console.log("kwtgToDelete");
    console.log(kwtgToDelete);

  }

}
