import { Component, OnInit } from '@angular/core';
import { _order , _orderGetAll } from '../models/model';
import { KeywordService } from '../services/keyword.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  dataSource: _order[] =[];
  dataSourceGetAll: _orderGetAll[] =[];
  displayedColumns: string[] = ['name', 'sku', 'cantidad'];
  login : boolean
  userid : string
  main: boolean;
  order: boolean;
  table: boolean;
  constructor(
    private keywordService: KeywordService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.main= true;
    this.order= false;
    this.table= false;
    this.getParams();
  }


  getParams() {

   const login = sessionStorage.getItem('userlogin');

    if (login != "" && login != null ){
      this.userid = login;
      this.login=true;

    }

  }

  async showtable() {
    this.main= false;
    this.order= false;
    this.table= true;
    this.getorderbyuseridBack();
  }

  async mainmenu() {
   
    this.main= true;
    this.order= false;
    this.table= false;
  }


  async starbuy() {
    this.main= false;
    this.order= true;
    this.table= false;
    this.getAllProductBack();
  }

  async GenerateOrder(e){
 
    await this.GenerateOrderBack(e.id,"1" );
    // se actualiza la bd 
    this.messageService.push('', 'se agrego a la order' , 'warning');
  }



  async getorderbyuseridBack(){

    return new Promise(async (resolve, reject) => {

     await this.keywordService.getOrderbyUserID(this.userid).subscribe((res: any) => {

      console.log("product_id 1");
      console.log(res);

      if (res === null || res === "null" ) {
        console.log("product_id 3");
        this.messageService.push('', 'Actualmente no se encontraron datos' , 'warning');
      }else{

        if (res.length === undefined) {
          this.getproductbyidBack( res.product_id);
        }else{
            for (let i = 0; i < res.length; i++) {
              this.getproductbyidBack( res[i].product_id);
            }
        }
      }
        resolve(res);
      });

    });

  }

  async getproductbyidBack(productID){

    return new Promise(async (resolve, reject) => {

     await this.keywordService.getProductbyID(productID).subscribe((res: any) => {

      if (res === null || res === "null" ) {
        this.messageService.push('', 'errorrfdfdsfdfdfr' , 'warning');
      }else{


        if (res.length === undefined) {
          this.dataSource.push({ id: res.id ,name: res.name, sku: res.sku, cantidad : res.amount});
        }else{
            for (let i = 0; i < res.length; i++) {
              this.dataSource.push({ id: res[i].id ,name: res[i].name, sku: res[i].sku, cantidad : res[i].amount});
            }
        }
      }
        resolve(res);
      });

    });

  }

  async getAllProductBack( ){

    return new Promise(async (resolve, reject) => {

     await this.keywordService.getAllProduct().subscribe((res: any) => {

      if (res === null || res === "null" ) {
        this.messageService.push('', 'Actualmente no tenemos productos disponible' , 'warning');
      }else{
        this.dataSourceGetAll=[];
        for (let i = 0; i < res.length; i++) {
          this.dataSourceGetAll.push({ id: res[i].id , userID: res[i].userID ,  name: res[i].name, sku: res[i].sku, cantidad : res[i].amount});
        }
      }
        resolve(res);
      });

    });

  }

  async GenerateOrderBack(_product_id  , _cantidad){

    return new Promise(async (resolve, reject) => {
      const sendData = {  user_id: this.userid, product_id: _product_id , cantidad: _cantidad  };  
      await this.keywordService.generateOrder(sendData).subscribe((res: any) => {
 
       if (res === null || res === "null" ) {
         this.messageService.push('', 'No se puedo realiar la orden' , 'warning');
       } 
         resolve(res);
       });

     });

  }


}
